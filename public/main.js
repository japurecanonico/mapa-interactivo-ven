$(document).ready(function () {
  $(".collapsible").collapsible();

  var color_teal = "#009688",
    color_teal_accent = "#00bfa5";
});

$(".edo").click(function () {
  $(".edo").removeClass("active");
  $(this).addClass("active");
});

$(".edo-anzoategui").click(function () {
  $("#card-header").html("Anzoátegui");
  $("#card-collapsible").load('anzoategui.html');
});

$(".edo-aragua").click(function () {
  $("#card-header").html("Aragua");
  $("#card-collapsible").load('aragua.html');
});

$(".edo-barinas").click(function () {
  $("#card-header").html("Barinas");
  $("#card-collapsible").load('barinas.html');
});

$(".edo-bolivar").click(function () {
  $("#card-header").html("Bolívar");
  $("#card-collapsible").load('bolivar.html');
});

$(".edo-carabobo").click(function () {
  $("#card-header").html("Carabobo");
  $("#card-collapsible").load('carabobo.html');
});

$(".edo-dttocapital").click(function () {
  $("#card-header").html("Dtto. Capital");
  $("#card-collapsible").load('dttocapital.html');
});

$(".edo-esparta").click(function () {
  $("#card-header").html("Nueva Esparta");
  $("#card-collapsible").load('esparta.html');
});

$(".edo-guarico").click(function () {
  $("#card-header").html("Guarico");
  $("#card-collapsible").load('guarico.html');
});

$(".edo-lara").click(function () {
  $("#card-header").html("Lara");
  $("#card-collapsible").load('lara.html');
});

$(".edo-merida").click(function () {
  $("#card-header").html("Mérida");
  $("#card-collapsible").load('merida.html');
});

$(".edo-miranda").click(function () {
  $("#card-header").html("Miranda");
  $("#card-collapsible").load('miranda.html');
});

$(".edo-monagas").click(function () {
  $("#card-header").html("Monagas");
  $("#card-collapsible").load('monagas.html');
});

$(".edo-portuguesa").click(function () {
  $("#card-header").html("Portuguesa");
  $("#card-collapsible").load('portuguesa.html');
});

$(".edo-tachira").click(function () {
  $("#card-header").html("Táchira");
  $("#card-collapsible").load('tachira.html');
});

$(".edo-trujillo").click(function () {
  $("#card-header").html("Trujillo");
  $("#card-collapsible").load('trujillo.html');
});

$(".edo-laguaira").click(function () {
  $("#card-header").html("La Guaira");
  $("#card-collapsible").load('laguaira.html');
});

$(".edo-zulia").click(function () {
  $("#card-header").html("Zulia");
  $("#card-collapsible").load('zulia.html');
});

console.log('%cAlto!', 'color: red;font-size:2rem;font-weight:bold;');
console.log('%cEsta es una función del navegador destinada a desarrolladores. No robar (7_7)', 'font-size:1rem;');